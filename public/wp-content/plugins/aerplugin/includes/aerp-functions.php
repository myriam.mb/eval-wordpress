<?php
/*
 * Menu
 */

// Hook the 'admin_menu' action hook, run the function named 'aerp_menu()'
add_action( 'admin_menu', 'aerp_menu' );
 
// link pour menu

function aerp_menu()
{
    add_menu_page(
        'Annuaire des entreprises de votre réseau', // Title of the page
        'Annuaire', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        'annuaires',
        'fiche' // The 'slug' - file to display when clicking the link
    );
}

//pour afficher la page du plugin annuraire
function fiche(){
    include 'aerp-page.php';
}

// pour les données
//[show]
function show_func($atts)
{
    global $wpdb;

// Interrogation de la base de données
$results = $wpdb->get_results(
    $wpdb->prepare(
        "SELECT * FROM {$wpdb->prefix}annuaire", OBJECT
        )
    );

    // pour parcourir les données
    foreach ($results as $entreprise) {
        echo 
            "<li>L'entreprise {$entreprise -> nom_entreprise} à {$entreprise -> localisation_entreprise}.</li>
                <p> Contact : {$entreprise -> prenom_contact} {$entreprise -> nom_contact} {$entreprise -> mail_contact}</p>";
    }
}

add_shortcode( 'show', 'show_func' );



