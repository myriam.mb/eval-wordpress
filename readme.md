## Contexte du projet

On vous demande de mettre en oeuvre un site basé sur Wordpress, sur lequel vous allez développer une fonctionnalité spécifique, sous forme de plugin. Le menu devra permettre de naviguer sur l'accueil, l'annuaire, ainsi qu'une page d'information sur l'identité de l'auteur du site. La charte graphique est "gris anthracite", pas de logo. La page annuaire présente une liste des entreprises que vous avez croisées dans votre parcours professionnel, dans les domaines de la tech et du numérique, et dont les données sont stockées dans une table MySQL (script fourni).

- Bonus backend : ajoutez une interface graphique dans le backoffice Wordpress, qui permette d'éditer les entreprises.
- Bonus frontend : prévoyez une pagination, ainsi que des fonctionnalités de recherche et de tri.
- Précision : l'utilisation d'un plugin déjà existant est (évidemment) interdit !

## Installation

Requiert Apache
- Ajouter une adresse dans le fichier local hosts ( etc/hosts)
- Créer un fichier conf dans le dossier sites-available ( etc/apache2/sites-available) pour le server
- Relancer apache2 ou faire l'installation de wordpress avant
- Créer une base de donnée mysql et lier
- Lancer l'instal

## Ressources

- Installation : https://doc.ubuntu-fr.org/wordpress
- Theme enfant : https://wpformation.com/theme-enfant-wordpress/
